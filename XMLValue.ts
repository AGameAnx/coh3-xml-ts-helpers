/* eslint-disable @typescript-eslint/no-explicit-any */

import XMLNode from './XMLNode';
import type EssenceInstance from './Instance';

export default class XMLValue extends XMLNode
{
	public value: string;

	public constructor(xml: any, nodeName: string, dataPath: (string|number)[] = [], instanceReference?: EssenceInstance) {
		super(xml, nodeName, dataPath, instanceReference);
		this.value = this.__attributes.value;
	}

	public clone(parentNode: XMLNode): XMLValue {
		const result = XMLValue.customValue(this.__nodeName);
		result.__attributes = Object.assign({}, this.__attributes);
		result.setParent(parentNode);
		result.value = this.value;
		return result;
	}

	static customValue(nodeName: string, name?: string, value: string | null = '', isOverride: boolean = true, parent?: XMLNode) {
		const attributes: any = {
			name: name
		};
		if (value !== null) {
			attributes.value = value;
		}
		if (isOverride) {
			attributes.overrideParent = 'True';
		}
		const result = new XMLValue({ [':@']: attributes, [nodeName]: [] }, nodeName);
		if (parent) { result.setParent(parent); }
		return result;
	}

	public toXML(): any {
		this.__attributes.value = this.value;
		return {[':@']: this.__attributes};
	}

	public toString(): string {
		return `${this.__nodeName}: ${this.value}`;
	}
}
