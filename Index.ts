import Instance from '../xml-ts-batch-process/Instance';
import XMLBool from './XMLBool';
import XMLList from './XMLList';
import XMLNode from './XMLNode';
import XMLNumber from './XMLNumber';
import XMLValue from './XMLValue';
import XMLVariant from './XMLVariant';

export
{
	Instance,
	XMLBool,
	XMLList,
	XMLNode,
	XMLNumber,
	XMLValue,
	XMLVariant
};
