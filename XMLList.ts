/* eslint-disable @typescript-eslint/no-explicit-any */

import XMLNode from './XMLNode';
import type EssenceInstance from './Instance';

export default class XMLList extends XMLNode
{
	public list: XMLNode[];

	public constructor(xml: any, nodeName: string, dataPath: (string|number)[] = [], instanceReference?: EssenceInstance) {
		super(xml, nodeName, dataPath, instanceReference, false);
		this.initData(xml[nodeName]);
	}

	public clone(parentNode: XMLNode): XMLList {
		const result = XMLList.customList(this.__nodeName);
		result.__attributes = this.__attributes;
		result.__nodeName = this.__nodeName;
		result.setParent(parentNode);
		for (const node of this.list) {
			result.list.push(node.clone(result));
		}
		return result;
	}

	static customList(nodeName: string, name?: string, value: XMLNode[] = []): XMLList {
		return new XMLList({
			[':@']: {
				name: name
			},
			[nodeName]: value.map(val => val.toXML())
		}, nodeName);
	}

	public append(node: XMLNode) {
		this.list.push(node);
		node.setOverridesParent();
		let list_item_id: string;
		do {
			list_item_id = (Math.random() * 4000000 - 2000000).toFixed(0);
		} while (XMLNode.disallowed_list_item_ids.indexOf(list_item_id) >= 0);
		node.__attributes['List.ItemID'] = list_item_id;
		node.__attributes['List.ListAction'] = 'Append';
	}

	public initData(xml: any[]): void {
		this.list = [];
		for (const dataEntry of xml) {
			const attributes = dataEntry[':@'];
			const nodeName = XMLNode.getNodeName(dataEntry);
			const node = XMLNode.createTyped(dataEntry, nodeName, this.__dataPath.concat([attributes.value ? attributes.value : this.list.length]), this.__instanceReference);
			this.list.push(node);
		}
	}

	public toXML(): any {
		const result: any = {[':@']: this.__attributes};
		for (const node of this.list) {
			const nodeName: string = node.__nodeName;
			const val: any = node.toXML();
			if (result[nodeName]) {
				result[nodeName].push(val);
			} else {
				result[nodeName] = [val];
			}
		}
		return result;
	}
}
