/* eslint-disable @typescript-eslint/no-explicit-any */

import XMLNode from './XMLNode';
import type EssenceInstance from './Instance';

export default class XMLBool extends XMLNode
{
	public value?: boolean;

	public constructor(xml: any, nodeName: string, dataPath: (string|number)[] = [], instanceReference?: EssenceInstance) {
		super(xml, nodeName, dataPath, instanceReference);
		this.value = this.__attributes.value ? this.__attributes.value.toLocaleLowerCase() == 'true' : null;
	}

	public clone(parentNode: XMLNode): XMLBool {
		const result = XMLBool.customBool();
		result.__attributes = Object.assign({}, this.__attributes);
		result.__nodeName = this.__nodeName;
		result.setParent(parentNode);
		result.value = this.value;
		return result;
	}

	static customBool(name?: string, value: boolean | null = false, nodeName?: string, isOverride: boolean = true, parent?: XMLNode) {
		if (!nodeName) nodeName = 'bool';
		const attributes: any = {
			name: name
		};
		if (value !== null) {
			attributes.value = value;
		}
		if (isOverride) {
			attributes.overrideParent = 'True';
		}
		const result = new XMLBool({ [':@']: attributes, [nodeName]: [] }, nodeName);
		if (parent) { result.setParent(parent); }
		return result;
	}

	public toXML(): any {
		if (this.value !== null)
			this.__attributes.value = this.value ? 'True' : 'False';
		return {[':@']: this.__attributes};
	}

	public toString(): string {
		return `${this.__nodeName}=${this.value}`;
	}
}
