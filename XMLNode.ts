/* eslint-disable @typescript-eslint/no-explicit-any */

import { XMLList, XMLValue, XMLBool, XMLNumber, XMLVariant } from './Index';
import EssenceInstance from './Instance';

export default class XMLNode
{
	public static disallowed_list_item_ids: string[] = [];

	static fields: string[] = ['__attributes', '__nodeName', '__dataPath', '__instanceReference'];

	public __nodeName: string;
	public __attributes: {[key: string]: string} = {};
	public __dataPath: (string|number)[];
	public __instanceReference?: EssenceInstance;

	public constructor(xml: any, nodeName: string, dataPath: (string|number)[] = [], instanceReference?: EssenceInstance, initData: boolean = true) {
		this.__nodeName = nodeName;
		this.__attributes = xml[':@'];
		if (this.__attributes['List.ItemID'] && XMLList.disallowed_list_item_ids.indexOf(this.__attributes['List.ItemID']) >= 0) {
			let list_item_id: string;
			do {
				list_item_id = (Math.random() * 4000000 - 2000000).toFixed(0);
			} while (XMLList.disallowed_list_item_ids.indexOf(list_item_id) >= 0);
			this.__attributes['List.ItemID'] = list_item_id;
		}
		this.__dataPath = dataPath;
		this.__instanceReference = instanceReference;
		if (initData) {
			this.initData(xml[nodeName]);
		}
	}

	public clone(parentNode: XMLNode, cloneFields: boolean = true): XMLNode {
		const result: any = XMLNode.custom(this.__nodeName);
		result.__attributes = Object.assign({}, this.__attributes);
		result.__nodeName = this.__nodeName;
		result.setParent(parentNode);
		if (cloneFields) {
			for (const key in this) {
				if (XMLNode.fields.indexOf(key) < 0) {
					let node: any;
					const val: any = this[key];
					if (typeof val == 'boolean') {
						node = XMLBool.customBool(key, val);
						node.setParent(result);
					} else if (typeof node == 'number') {
						node = XMLNumber.customNumber(key, val);
						node.setParent(result);
					} else {
						node = val.clone(result);
					}
					result[key] = node;
				}
			}
		}
		return result;
	}

	static custom(nodeName: string, name?: string, isOverride: boolean = true, parent?: XMLNode) {
		const attributes: any = {
			name: name
		};
		if (isOverride) {
			attributes.overrideParent = 'True';
		}
		const result = new XMLNode({ [':@']: attributes, [nodeName]: [] }, nodeName);
		if (parent) { result.setParent(parent); }
		return result;
	}

	static getNodeName(xml: any):string {
		for (const key in xml) {
			if (key != ':@')
				return key;
		}
		return null;
	}

	static createTyped(xml: any, nodeName: string, dataPath: (string|number)[] = [], instanceReference?: EssenceInstance): XMLNode {
		switch (nodeName) {
		case 'float':
			return new XMLNumber(xml, nodeName, dataPath, instanceReference);
		case 'bool':
			return new XMLBool(xml, nodeName, dataPath, instanceReference);
		case 'list':
			return new XMLList(xml, nodeName, dataPath, instanceReference);
		case 'uniqueid':
		case 'int':
		case 'locstring':
			return new XMLNumber(xml, nodeName, dataPath, instanceReference);
		case 'instance_reference':
		case 'string':
		case 'file':
		case 'colour':
		case 'enum':
		case 'state_tree_node':
			return new XMLValue(xml, nodeName, dataPath, instanceReference);
		case 'variant':
			return new XMLVariant(xml, nodeName, dataPath, instanceReference);
		case 'group':
		case 'sound':
		case 'enum_table':
		case 'template_reference':
		case 'normalized_response':
			return new XMLNode(xml, nodeName, dataPath, instanceReference);
		default: {
			console.log(`Encountered unknown nodeName: ${nodeName}`);
			return new XMLNode(xml, nodeName, dataPath, instanceReference);
		}
		}
	}

	public initData(xml: any[]): void {
		for (const dataEntry of xml) {
			const attributes = dataEntry[':@'];
			const nodeName = XMLNode.getNodeName(dataEntry);
			const node = XMLNode.createTyped(dataEntry, nodeName, this.__dataPath.concat([attributes.name]), this.__instanceReference);
			if (node !== null)
				this[attributes.name] = node;
		}
	}

	public setParent(parentNode: XMLList | XMLNode): void {
		this.__instanceReference = parentNode.__instanceReference;
		let pathEntry: string | number;
		if (parentNode instanceof XMLList) {
			pathEntry = parentNode.__attributes.value ? parentNode.__attributes.value : parentNode.list.findIndex((value) => value == this);
		} else {
			pathEntry = this.__attributes.name;
		}
		this.__dataPath = parentNode.__dataPath.concat([pathEntry]);
	}

	public setOverridesParent(val?: boolean): void {
		if (val === undefined) {
			delete this.__attributes['overrideParent'];
		} else {
			this.__attributes['overrideParent'] = val ? 'True' : 'False';
		}
	}

	public toXML(): any {
		const result: any = {[':@']: this.__attributes};
		for (const key in this) {
			if (XMLNode.fields.indexOf(key) < 0) {
				const node: any = this[key];
				let nodeName: string;
				let val: any;
				if (typeof node == 'boolean') {
					val = {[':@']: {name: key, value: node ? 'True' : 'False'}};
					nodeName = 'bool';
				} else if (typeof node == 'number') {
					val = {[':@']: {name: key, value: node.toString()}};
					nodeName = 'float';
				} else {
					val = node.toXML();
					nodeName = node.__nodeName;
				}
				if (result[nodeName]) {
					result[nodeName].push(val);
				} else {
					result[nodeName] = [val];
				}
			}
		}
		return result;
	}
}
