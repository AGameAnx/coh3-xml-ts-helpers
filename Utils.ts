/* eslint-disable @typescript-eslint/no-explicit-any */

import * as fs from 'fs';
import { basePath } from '../xml-ts-batch-process/Settings';
import Instance from './Instance';
import * as EssenceXML from './Index';
import { print } from '../xml-ts-batch-process/Print';
import * as IH from '../xml-ts-batch-process/InstancesHelper';
import EssenceInstance from './Instance';
import assert from 'assert';

export function clearInstances(path: string = ''): void {
	if (fs.existsSync(basePath + path)) {
		fs.rmSync(basePath + path, { recursive: true });
	}
}
export async function clearInstancesAsync(path: string = ''): Promise<void> {
	if (fs.existsSync(basePath + path)) {
		return fs.promises.rm(basePath + path, { recursive: true });
	}
}

export const l = (path: string): Instance => Instance.fromPath(Instance, path);

export async function asyncList(list: string[], fn: (entry: string) => Promise<void>): Promise<void[]> {
	const promises: Promise<void>[] = [];
	for (const entry of list) {
		promises.push(fn(entry));
	}
	return Promise.all(promises);
}

export function parentValue(t: any, dataPath?: (string|number)[]) {
	if (!t.__instanceReference) return undefined;
	if (!dataPath) dataPath = t.__dataPath;
	let val = t.__instanceReference.data;
	for (const pathElement of dataPath) {
		if (val instanceof EssenceXML.XMLList) {
			if (typeof pathElement == 'string')
				val = listFindByValue(val.list, pathElement);
			else
				val = val.list[pathElement];
		} else {
			val = val[pathElement];
		}
		if (val === undefined) {
			break;
		}
	}
	if (val === undefined) {
		return parentValue(t.__instanceReference.data[dataPath[0]], dataPath);
	}
	return val;
}

function ensureFromReference(t: any, field: string | number): boolean {
	const val = get(t, field, false);
	if (val) {
		t[field] = val.clone(t, val.__nodeName != 'template_reference');
		return true;
	}
	return false;
}

export function ensure(t: any, field: string | number) {
	if (!t[field]) {
		ensureFromReference(t, field);
	}
	assert(t[field]);
	return t[field];
}
export function ensureCustom(t: any, nodeName: string, field: string | number) {
	if (!t[field] && !ensureFromReference(t, field)) {
		t[field] = EssenceXML.XMLNode.custom(nodeName, field.toString());
	}
	return t[field];
}
export function ensureNumber(t: any, field: string | number, value: number | null = 0, override: boolean = true) {
	if (!t[field] && !ensureFromReference(t, field)) {
		t[field] = EssenceXML.XMLNumber.customNumber(field.toString(), value, null, override);
	}
	return t[field];
}
export function ensureBool(t: any, field: string | number, value: boolean | null = false, override: boolean = true) {
	if (!t[field] && !ensureFromReference(t, field)) {
		t[field] = EssenceXML.XMLBool.customBool(field.toString(), value, null, override);
	}
	return t[field];
}
export function ensureValue(t: any, nodeName: string, field: string | number, value: string | null = '', override: boolean = true) {
	if (!t[field] && !ensureFromReference(t, field)) {
		t[field] = EssenceXML.XMLValue.customValue(nodeName, field.toString(), value, override);
	}
	return t[field];
}
export function ensureListEntry(t: EssenceXML.XMLList, entryName: string, dataPath?: (string|number)[], s: EssenceXML.XMLList = null): any {
	if (!dataPath) dataPath = t.__dataPath;
	if (!s) s = t;
	const listFindResult = listFindByValue(t.list, entryName);
	if (listFindResult) return listFindResult;
	if (t.__instanceReference && dataPath) {
		let val = parentValue(s, dataPath);
		if (val instanceof EssenceXML.XMLList) {
			let notFound = true;
			for (const listElement of val.list) {
				if (listElement.__attributes.value == entryName) {
					val = listElement;
					notFound = false;
					break;
				}
			}
			if (notFound) {
				return val.__instanceReference ? ensureListEntry(t, entryName, dataPath, val) : null;
			}
		}
		if (val !== undefined) {
			const isTemplate = val.__nodeName == 'template_reference';
			const newVal = val.clone(t, !isTemplate);
			newVal.__dataPath[newVal.__dataPath.length-1] = entryName;
			if (isTemplate) {
				newVal.__attributes['List.ParentItemID'] = val.__attributes['List.ItemID'];
			}
			t.list.push(newVal);
			return newVal;
		}
	}
	return null;
}

export function listFindByValue(list: any, value: string) {
	for (const entry of list) {
		if (entry.__attributes.value == value)
			return entry;
	}
	return undefined;
}

export const shouldSkip = (name: string): boolean => name.substring(name.length-3) == '_sp' || name.indexOf('_sp_') >= 0 || name.substring(name.length-3) == '_dummy' || name.indexOf('_dummy_') >= 0;

export const round = (val: number, to: number = 0): number => Math.round(val * 10 ** to) / 10 ** to;

export function get(t: any, field: string | number, returnValue: boolean = true):any {
	const val = t[field] ?? parentValue(t, t.__dataPath.concat([field]));
	const isValueObj = val !== null && typeof val == 'object' && 'value' in val;
	if (isValueObj && returnValue) {
		return val.value;
	}
	return val;
}

export function modify(t: any, field: string | number, fn: (val: any) => any): void {
	const startValue = get(t, field);

	ensure(t, field);

	let val = t[field];

	const isValueObj = typeof val == 'object' && 'value' in val;
	if (isValueObj) {
		val = val.value;
	}

	const result = fn(val);
	print(`${field}: ${startValue} => ${result}`);

	if (!t[field].__attributes.overrideParent && (t.__nodeName == 'template_reference' || t.__instanceReference && t.__dataPath)) {
		t[field].__attributes.overrideParent = 'True';
	}

	if (isValueObj) {
		t[field].value = result;
	} else {
		t[field] = result;
	}
}

export function modifyMod(t: any, field: string, fn: (val: number) => number): void {
	modify(t, field, val => round(fn(val), 4));
}

export function process(
	fn: (instance: EssenceInstance) => void,
	dir: string = '',
	recursive: boolean = true,
	workOnOriginalInstances: boolean = true
): void {
	IH.get(EssenceInstance, dir, recursive, workOnOriginalInstances).forEach(instance => {
		instance.load();
		fn(instance);
		instance.unload();
	});
}

export function processFileList(
	paths: string[],
	fn: (instance: EssenceInstance) => void
): void {
	const instances: EssenceInstance[] = [];
	for (const path of paths) {
		instances.push(EssenceInstance.fromPath(EssenceInstance, path));
	}
	instances.forEach(instance => {
		instance.load();
		fn(instance);
		instance.unload();
	});
}

export function done(start: number): void {
	console.log(`Done! Elapsed: ${((performance.now() - start)/1000).toFixed(3)}s (${new Date().toISOString()})`);
}
