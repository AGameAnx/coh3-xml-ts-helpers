/* eslint-disable @typescript-eslint/no-explicit-any */

import XMLNode from './XMLNode';
import EssenceInstance from './Instance';

export default class XMLVariant extends XMLNode
{
	public constructor(xml: any, nodeName: string, dataPath: (string|number)[] = [], instanceReference?: EssenceInstance) {
		if (!instanceReference) {
			for (const i in xml[nodeName]) {
				const node = xml[nodeName][i];
				const variantNodeName = XMLNode.getNodeName(node);
				if (variantNodeName == 'instance_reference' && node[':@'].value.length > 0) {
					instanceReference = EssenceInstance.fromPath(EssenceInstance, node[':@'].value.replaceAll('\\', '/') + '.xml', true);
					instanceReference.load();
					break;
				}
			}
		}
		super(xml, nodeName, dataPath, instanceReference);
	}
}
