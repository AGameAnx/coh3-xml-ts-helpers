/* eslint-disable @typescript-eslint/no-explicit-any */

import Instance from '../xml-ts-batch-process/Instance';
import * as EssenceXML from './Index';

export default class EssenceInstance extends Instance
{
	public xmlCopy: any = {};
	public data: any;

	protected createData(): void {
		Object.assign(this.xmlCopy, this.xml);
		this.data = new EssenceXML.XMLNode(this.xml[0], 'instance');
		this.xmlInitial = JSON.stringify(this.data.toXML());
	}

	public updateXML(): void {
		this.xml = {instance: this.data.toXML()};
	}

	public hasChanges(): boolean {
		this.updateXML();
		return JSON.stringify(this.xml) != this.xmlInitial;
	}

	public load(): void {
		super.load();
		this.createData();
	}

	public async promise_load(): Promise<void> {
		super.load();
		this.createData();
	}

	public save(force: boolean = false): void {
		this.updateXML();
		super.save(force);
	}

	public async promise_save(force: boolean = false): Promise<void> {
		this.updateXML();
		return super.promise_save(force);
	}

	public saveAs(filename: string, fullPath: boolean = false): void {
		this.updateXML();
		super.saveAs(filename, fullPath);
	}

	public async promise_saveAs(filename: string, fullPath: boolean = false): Promise<void> {
		this.updateXML();
		return super.promise_saveAs(filename, fullPath);
	}
}
