/* eslint-disable @typescript-eslint/no-explicit-any */

import XMLNode from './XMLNode';
import type EssenceInstance from './Instance';
import assert from 'assert';

export default class XMLNumber extends XMLNode
{
	public value: number | null = null;

	public constructor(xml: any, nodeName: string, dataPath: (string|number)[] = [], instanceReference?: EssenceInstance) {
		super(xml, nodeName, dataPath, instanceReference);
		if (this.__attributes.value) {
			this.value = +this.__attributes.value;
			assert(!isNaN(this.value));
		}
	}

	public clone(parentNode: XMLNode): XMLNumber {
		const result = XMLNumber.customNumber();
		result.__attributes = Object.assign({}, this.__attributes);
		result.__nodeName = this.__nodeName;
		result.setParent(parentNode);
		result.value = this.value;
		return result;
	}

	static customNumber(name?: string, value: number | null = 0, nodeName?: string, isOverride: boolean = true, parent?: XMLNode) {
		if (!nodeName) nodeName = 'float';
		const attributes: any = {
			name: name
		};
		if (value !== null) {
			attributes.value = value;
		}
		if (isOverride) {
			attributes.overrideParent = 'True';
		}
		const result = new XMLNumber({ [':@']: attributes, [nodeName]: [] }, nodeName);
		if (parent) { result.setParent(parent); }
		return result;
	}
	
	public toXML(): any {
		if (this.value) {
			this.__attributes.value = this.value.toString();
		}
		return {[':@']: this.__attributes};
	}

	public toString(): string {
		return `${this.__nodeName}: ${this.value}`;
	}
}
